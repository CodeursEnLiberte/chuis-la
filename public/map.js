const fromHexString = hexString =>
  new Uint8Array(hexString.match(/.{1,2}/g).map(byte => parseInt(byte, 16)));

async function decrypt(key, iv, ciphertextHex) {
  let decoded = await window.crypto.subtle.decrypt(
    {
      name:  "AES-CBC",
      iv:fromHexString(iv)
    },
    key,
    fromHexString(ciphertextHex)
  );

  let dec = new TextDecoder();
  return dec.decode(decoded);
}

function importSecretKey(rawKey) {
  return window.crypto.subtle.importKey(
    "raw",
    rawKey,
    "AES-CBC",
    false,
    ["decrypt"]
  );
}

document.addEventListener('DOMContentLoaded', function(event) {
  const queryString = window.location.search
  const urlParams = new URLSearchParams(queryString)
  const id = urlParams.get('id')
  const rawKey = fromHexString(window.location.hash.substr(1))

  let map = L.map('map').setView([47, 2], 6)
  L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png ', {
    attribution:
      'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors',
    maxZoom: 18,
  }).addTo(map)

  let latestMarker = null
  let circle = null

  let trace = L.featureGroup().addTo(map)
  let history = L.featureGroup().addTo(trace)
  let polyline = L.polyline([]).addTo(trace)

  let checkbox = null

  let errorDiv = document.querySelector('#error')
  async function updatePosition() {
    const request = new Request(`/positions/${id}`, {
      method: 'GET',
    })
    const key = await importSecretKey(rawKey)
    const {iv, ciphertext} = await fetch(request).then(response => response.json())
    const rawData = await decrypt(key, iv, ciphertext)
    let data = JSON.parse(rawData)

    errorDiv.classList.remove('show')
    const latLng = [data.localization.latitude, data.localization.longitude]

    if (!circle) {
      circle = L.circle(latLng, { radius: data.localization.accuracy }).addTo(map)

      var legend = L.control({position: 'bottomright'})
      legend.onAdd = function (map) {
          var div = L.DomUtil.create('div', 'leaflet-control-layers-expanded')
          var label = L.DomUtil.create('label', 'label', div)
          label.innerText = 'Historique'

          checkbox = L.DomUtil.create('input', 'checkbox', label)
          checkbox.type = "checkbox"

          L.DomEvent.addListener(checkbox, 'change', () => {
            if (!checkbox.checked) {
              polyline.setLatLngs([])
              history.clearLayers()
            } else {
              polyline.addLatLng(latestMarker.getLatLng())
            }
          })

          return div
      }
      legend.addTo(map)
    } else {
      circle.setLatLng(latLng).setRadius(data.localization.accuracy)

      latestMarker.removeFrom(map)
      if (checkbox.checked) {
        latestMarker.setStyle({fillColor: 'white'})
        latestMarker.addTo(history)
        polyline.addLatLng(latLng)
      }
    }

    latestMarker = L.circleMarker(latLng, { radius: 5, fillColor: 'blue', color: 'blue', fillOpacity: 1, weight: 1}).addTo(map)
    latestMarker.bindPopup(data.date)
    map.fitBounds(circle.getBounds())
}
  updatePosition()
  setInterval(updatePosition, 5000)
})
