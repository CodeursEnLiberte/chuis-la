const toHexString = bytes =>
  bytes.reduce((str, byte) => str + byte.toString(16).padStart(2, '0'), '');

function generateToken() {
  let array = new Uint8Array(16); // 128 bits
  window.crypto.getRandomValues(array);
  return toHexString(array);
}

async function generateKey() {
  return await crypto.subtle.generateKey(
    { name: "AES-CBC", length: 256 },
    true,
    ['encrypt', 'decrypt']
  );
}

async function encrypt(key, blob) {
  const enc = new TextEncoder();
  const encoded = enc.encode(blob);
  const iv = window.crypto.getRandomValues(new Uint8Array(16));
  const ciphertext = await window.crypto.subtle.encrypt(
    { name: "AES-CBC", iv },
    key,
    encoded
  );

  return {
    iv: toHexString(iv),
    ciphertext: toHexString(new Uint8Array(ciphertext))
  }
}

async function processPosition(position, id, secret, key) {
  const myPosition = {
    latitude: position.coords.latitude,
    longitude: position.coords.longitude,
    accuracy: position.coords.accuracy
  };
  // myPosition.latitude += (Math.random()-0.5)/100
  // myPosition.longitude += (Math.random()-0.5)/100

  const now = new Date();
  logPosition(now, myPosition);
  const positionStr = JSON.stringify({localization: myPosition, date: now})

  const {iv, ciphertext} = await encrypt(key, positionStr)
  const blob = JSON.stringify({iv, ciphertext})
  const body = JSON.stringify({secret, blob})
  const request = new Request(`/positions/${id}`, {method: 'POST', body});
  fetch(request)
}

function updatePosition(id, secret, key) {
  navigator.geolocation.getCurrentPosition(
    (position) => { processPosition(position, id, secret, key) },
    (error) => { logError(error.message) },
    { enableHighAccuracy: true }
  )
}

let gIntervalIdentifier
let gMapURL

function startStop() {
  if(gIntervalIdentifier==null) {
    start()
  } else {
    stop()
  }
}

let gSecret // Secret shared by the publisher and the server. Do not share with followers
let gId // The identifier of the current tracking. Can be public
let gKey// The key is shared with followers. Do not share with the server
async function setupKeys() {
  if (gSecret != null) {
    return
  }
  gSecret = generateToken()
  gId = generateToken()
  gKey = await generateKey()
}

async function setupURL() {
  const exported = await crypto.subtle.exportKey('raw', gKey)
  const exportedHex = toHexString(new Uint8Array(exported))
  gMapURL = `${window.location.origin}/map.html?id=${gId}#${exportedHex}`
  const styledURL = `${window.location.origin}/map.html?id=<span style="color:#6495ed">${gId}</span>#<span style="color:#ff7f50">${exportedHex}</span>`
  document.getElementById('mapURL').innerHTML = styledURL
  document.getElementById('copier').disabled = false
}

async function start() {
  if (!("geolocation" in navigator)) {
    logError('Géolocalisation indisponible')
    return
  }
  
  await setupKeys()
  await setupURL()
  
  updatePosition(gId, gSecret, gKey)
  gIntervalIdentifier = setInterval(() => updatePosition(gId, gSecret, gKey), 5000)
  document.getElementById('status').innerText = "Partage en cours…"
  document.getElementById('startstop').innerText = "Arrêter le partage"
}

function stop() {
  clearInterval(gIntervalIdentifier)
  gIntervalIdentifier = null
  document.getElementById('status').innerText = null
  document.getElementById('startstop').innerText = "Démarrer le partage"
}

function copyURL() { 
  if (gMapURL==null) {
    return 
  }
  navigator.clipboard.writeText(gMapURL) 
  logText(new Date(), "URL copied")
}

function logPosition(date, position) {
  logText(date, `lat ${position.latitude}, lon ${position.longitude} (~${position.accuracy}m)`)
}

function logError(error) {
  logText(new Date(), `ERROR: ${error}`)
}

function logText(date, text) {
  const line = document.createElement("div");
  line.innerHTML = `<span style="color:gray" "title="${date.toISOString()}">${date.toLocaleTimeString()}</span> ${text}`

  const logger = document.getElementById('logs')
  const lines = logger.children
  logger.insertBefore(line, lines[0])
  while (lines.length > 1000) { logger.removeChild(lines[lines.length-1]) }
}
