install: setup_lint generate_self_signed_certificate ## Install dependencies	

build: ## Build
	cargo build
	
run: ## Run
	cargo run
	
lint_fmt: ## Run fmt
	cargo fmt --all -- --check

lint_clippy: ## Run clippy
	cargo clippy -- -D warnings

lint: lint_fmt lint_clippy ## Run all linters

setup_lint: ## Add components for linters
	rustup component add rustfmt
	rustup component add clippy	

test: ## Execute backend tests
	cargo test

deploy: ## Deploy to clever cloud
	clever deploy
		
generate_self_signed_certificate: ## Setup self-signed certificate
	mkdir .tls-self-signed
	openssl req -newkey rsa:2048 -new -nodes -x509 -days 3650 -keyout .tls-self-signed/key.pem -out .tls-self-signed/cert-chain.pem

help: ## Display available commands
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'

.PHONY: run lint_fmt lint_clippy lint setup_lint test deploy generate_self_signed_certificate help

.DEFAULT_GOAL := help
