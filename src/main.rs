#[macro_use]
extern crate rocket;
use rocket::{
    serde::{json::Json, Deserialize},
    State,
};
use std::collections::HashMap;
use std::sync::{Arc, Mutex};

#[derive(Responder)]
enum Error {
    #[response(status = 403)]
    AccessDenied(String),
    #[response(status = 404)]
    NotFound(String),
    #[response(status = 500)]
    Internal(String),
}

#[derive(Deserialize)]
struct Position {
    secret: String,
    blob: String,
}

type Positions = Arc<Mutex<HashMap<String, Position>>>;

#[get("/<id>")]
fn get_position(id: &str, positions: &State<Positions>) -> Result<String, Error> {
    let positions = positions
        .lock()
        .map_err(|err| Error::Internal(format!("Could not get lock on positions: {err}")))?;
    if let Some(position) = positions.get(id) {
        Ok(position.blob.clone())
    } else {
        Err(Error::NotFound(format!(
            "Could not find position with id {id}"
        )))
    }
}

#[post("/<id>", data = "<new_position>")]
fn post_position(
    id: &str,
    new_position: Json<Position>,
    positions: &State<Positions>,
) -> Result<(), Error> {
    let mut positions = positions
        .lock()
        .map_err(|err| Error::Internal(format!("Could not get lock on positions: {err}")))?;
    if let Some(position) = positions.get(id) {
        if position.secret == new_position.secret {
            positions.insert(id.to_owned(), new_position.0);
            Ok(())
        } else {
            Err(Error::AccessDenied(
                "Access denied: invalid secret".to_string(),
            ))
        }
    } else {
        positions.insert(id.to_owned(), new_position.0);
        Ok(())
    }
}

#[launch]
async fn rocket() -> _ {
    let positions: Positions = Arc::new(Mutex::new(HashMap::new()));
    rocket::build()
        .manage(positions)
        .mount("/", rocket::fs::FileServer::from("public/"))
        .mount("/positions", routes![get_position, post_position])
}
